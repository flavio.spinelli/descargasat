import os
import zipfile
import base64
import json
import webview
import sqlite3
import datetime
import time
import socket

import config

from lxml import etree

from cfdiclient import Autenticacion
from cfdiclient import SolicitaDescarga
from cfdiclient import VerificaSolicitudDescarga
from cfdiclient import Fiel
from cfdiclient import DescargaMasiva

from intelisis import Intelisis

database = 'database.db'





import os
import http.server
import socketserver
from threading import Thread

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
DIRECTORY = os.path.join(BASE_DIR, 'app/build/')


class API:
    def __init__(self):
        self.create_tables()
        self.intelisis = None

    def create_tables(self):
        conn = sqlite3.connect(database)
        c = conn.cursor()
        c.execute('''CREATE TABLE IF NOT EXISTS credentials
             (cer text, key text, password text, rfc text UNIQUE, folder text, company text, db_server text, db_name text, db_user text, db_password text)''')
        conn.close()
    
    def open_file_dialog(self, file_type):
        file_types = (f'Certificados (*.{file_type})',)
        return win.window.create_file_dialog(webview.OPEN_DIALOG, allow_multiple=True, file_types=file_types)

    def open_folder_dialog(self):
        return win.window.create_file_dialog(webview.FOLDER_DIALOG)

    def login(self, cer, key, password, rfc, folder, company, db_server, db_name, db_user, db_password):
        try:
            cer_der = open(cer, 'rb').read()
            key_der = open(key, 'rb').read()

            fiel = Fiel(cer_der, key_der, password)
            auth = Autenticacion(fiel)
            token = auth.obtener_token()

            conn = sqlite3.connect(database)
            c = conn.cursor()
            c.execute(f"INSERT INTO credentials VALUES ('{cer}','{key}','{password}','{rfc}','{folder}', '{company}', '{db_server}', '{db_name}', '{db_user}', '{db_password}')")
            conn.commit()
            conn.close()

            return token
        except Exception as e:
            print('error: ', str(e))
            return False

    def logout(self):
        try:
            conn = sqlite3.connect(database)
            c = conn.cursor()
            c.execute(f"DELETE FROM credentials")
            conn.commit()
            conn.close()
            return True
        except:
            return False

    def get_credentials(self):
        conn = sqlite3.connect(database)
        c = conn.cursor()
        query = c.execute(f"SELECT cer, key, password, rfc, folder, company, db_server, db_name, db_user, db_password FROM credentials")
        data = c.fetchall()
        conn.close()
        return data

    def get_token(self, fiel):
        auth = Autenticacion(fiel)
        token = auth.obtener_token()

        return token

    def get_fiel(self):
        credentials = self.get_credentials()

        cer = credentials[0][0]
        key = credentials[0][1]
        password = credentials[0][2]
        rfc = credentials[0][3]

        cer_der = open(cer, 'rb').read()
        key_der = open(key, 'rb').read()

        fiel = Fiel(cer_der, key_der, password)

        return fiel

    def solicita_descarga(self, tipo='', fecha_inicial='', fecha_final='', rfc_receptor=''):
        fiel = self.get_fiel()

        credentials = self.get_credentials()
        rfc = credentials[0][3]

        descarga = SolicitaDescarga(fiel)

        token = self.get_token(fiel)

        fecha_inicial = datetime.datetime.strptime(fecha_inicial, '%Y-%m-%d %H:%M:%S')
        fecha_final = datetime.datetime.strptime(fecha_final, '%Y-%m-%d %H:%M:%S')

        if tipo == 'emitidas':
            result = descarga.solicitar_descarga(token, rfc, fecha_inicial, fecha_final, rfc_emisor=rfc, tipo_solicitud='CFDI')
        elif tipo == 'recibidas':
            result = descarga.solicitar_descarga(token, rfc, fecha_inicial, fecha_final, rfc_receptor=rfc, tipo_solicitud='CFDI')

        print('solicita_descarga', result)
        return result
        return result['id_solicitud']

    def verifica_solicitud_de_descarga(self, id_solicitud):
        fiel = self.get_fiel()

        v_descarga = VerificaSolicitudDescarga(fiel)

        token = self.get_token(fiel)

        credentials = self.get_credentials()
        rfc_solicitante = credentials[0][3]

        result = v_descarga.verificar_descarga(token, rfc_solicitante, id_solicitud)

        print('verifica_solicitud_de_descarga', result)

        return result

    def descarga(self, id_paquete):
        try:
            fiel = self.get_fiel()
            token = self.get_token(fiel)

            credentials = self.get_credentials()
            rfc_solicitante = credentials[0][3]
            folder = credentials[0][4]

            descarga = DescargaMasiva(fiel)
            descarga = descarga.descargar_paquete(token, rfc_solicitante, id_paquete)

            dir_name = folder
            file_name = f'{id_paquete}.zip'

            with open(f'{dir_name}/{file_name}', 'wb') as fp:
                fp.write(base64.b64decode(descarga['paquete_b64']))

            print('descarga: ', id_paquete)
            return {
                'dir_name': dir_name,
                'file_name': file_name
            }
        except Exception as e:
            print('err descarga: ', str(e))
            return False


    def unzip_file(self, file_name, dir_name):
        try:
            full_path = dir_name +'/'+ file_name
            zip_ref = zipfile.ZipFile(full_path)
            zip_ref.extractall(full_path.replace('.zip', ''))
            zip_ref.close()
            os.remove(full_path)
            return True
        except Exception as e:
            print('err unzip_file: ', str(e))
            return str(e)

    def read_and_insert(self, dir_name):
        prefix_map = {"cfdi": "http://www.sat.gob.mx/cfd/3"}
        try:
            credentials = self.get_credentials()
            company = credentials[0][5]
            db_server = credentials[0][6]
            db_name = credentials[0][7]
            db_user = credentials[0][8]
            db_password = credentials[0][9]

            self.intelisis = Intelisis(
                server=db_server,
                database=db_name,
                uid=db_name,
                pwd=db_password
            )

            for item in os.listdir(dir_name):
                try:
                    doc = etree.parse(dir_name +'/'+ item)

                    with open(dir_name +'/'+ item, 'r') as file:
                        xmlstr = file.read().replace('\n', '')


                    empresa = company
                    ruta = None
                    documento = xmlstr
                    tipo = doc.getroot().get('TipoDeComprobante') 
                    uuid = doc.find("cfdi:Complemento", prefix_map).getchildren()[0].get('UUID')
                    rfc_emisor =  doc.find("cfdi:Emisor", prefix_map).get('Rfc')
                    rfc_receptor =  doc.find("cfdi:Receptor", prefix_map).get('Rfc')
                    fecha_timbrado = doc.find("cfdi:Complemento", prefix_map).getchildren()[0].get('FechaTimbrado')
                    monto = doc.getroot().get('Total')
                    moneda = doc.getroot().get('Moneda')
                    tipo_cambio = doc.getroot().get('TipoCambio')
                    nombre = item
                    mon_xml = None
                    cadenaoriginal = None
                    validacion_estructura = None
                    certificado_reportado = None
                    certificado_usado = None
                    nombre_emisor = doc.find("cfdi:Emisor", prefix_map).get('Nombre')

                    self.intelisis.connect()
                    insert = self.intelisis.insert(
                        empresa=empresa,
                        ruta=ruta,
                        documento=documento,
                        tipo=tipo,
                        uuid=uuid,
                        rfc_emisor=rfc_emisor,
                        rfc_receptor=rfc_receptor,
                        fecha_timbrado=fecha_timbrado,
                        monto=monto,
                        moneda=moneda,
                        tipo_cambio=tipo_cambio,
                        nombre=nombre,
                        mon_xml=mon_xml,
                        cadenaoriginal=cadenaoriginal,
                        validacion_estructura=validacion_estructura,
                        certificado_reportado=certificado_reportado,
                        certificado_usado=certificado_usado,
                        nombre_emisor=nombre_emisor
                    )
                except:
                    continue

            return True
        except Exception as e:
            print('err read_and_insert: ', str(e))
            return str(e)


class Window:
    def __init__(self, debug, title, url, width, height):
        self.title = title
        self.url = url
        self.debug = debug
        self.width = width
        self.height = height

    def start(self):
        api = API()

        conn = sqlite3.connect(database)
        c = conn.cursor()
        query = c.execute(f"SELECT * FROM credentials")
        
        path = '/'
        if query.fetchone():
            path = '/facturas-emitidas'


        conn.close()

        self.window = webview.create_window(title=self.title, url=self.url + path, width=self.width, height=self.height, js_api=api)
        win = self.window
        webview.start(debug=self.debug)

class Handler(http.server.SimpleHTTPRequestHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, directory=DIRECTORY, **kwargs)

def start_server(port):
    with socketserver.TCPServer(('', port), Handler) as httpd:
        httpd.serve_forever()

def next_free_port( port=8000, max_port=8999 ):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    while port <= max_port:
        try:
            sock.bind(('', port))
            sock.close()
            return port
        except OSError:
            port += 1
    raise IOError('no free ports')

if __name__ == "__main__":
    port = next_free_port()
    thread2 = Thread(target = start_server, args=(port,))
    thread2.start()
    print("serving at port", port)

    win = Window(debug=True, title='Descarga SAT', url=f'http://localhost:{port}', width=1000, height=700)
    win.start()