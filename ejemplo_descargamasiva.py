# -*- coding: utf-8 -*-
from cfdiclient import DescargaMasiva
from cfdiclient import Fiel
import os
from cfdiclient.autenticacion import Autenticacion

##
## Constantes de Loggin
##
RFC = 'GACJ8504306G7'
FIEL_CER = '00001000000506197164.cer'
FIEL_KEY = 'Claveprivada_FIEL_GACJ8504306G7_20210117_114311.key'
FIEL_PAS = '8nalgwpJ$'
PATH = 'certificados/'

cer_der = open(os.path.join(PATH, FIEL_CER), 'rb').read()
key_der = open(os.path.join(PATH, FIEL_KEY), 'rb').read()

fiel = Fiel(cer_der, key_der, FIEL_PAS)

auth = Autenticacion(fiel)
token = auth.obtener_token()

descarga = DescargaMasiva(fiel)


rfc_solicitante = 'GACJ8504306G7'
id_paquete = '10B82F09-811C-4C31-B096-532610440679_01'
result = descarga.descargar_paquete(token, rfc_solicitante, id_paquete)
print(result)