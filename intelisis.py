import pyodbc

class Intelisis:
    def __init__(self, server, database, uid, pwd):
        self.driver = '{SQL Server}'
        self.server = server
        self.database = database
        self.uid = uid
        self.pwd = pwd

    def connect(self):
        return pyodbc.connect(
            f'Driver={self.driver};'
            f'Server={self.server};'
            f'Database={self.database};'
            f'uid={self.uid};'
            f'pwd={self.pwd}'
        )

    def insert(self, empresa, ruta, documento, tipo, uuid, rfc_emisor, rfc_receptor, fecha_timbrado, monto, moneda, tipo_cambio, nombre, mon_xml, cadenaoriginal, validacion_estructura, certificado_reportado, certificado_usado, nombre_emisor):
        try:
            conn = self.connect()
            cursor = conn.cursor()
            cursor.execute(f"""
                INSERT INTO CFDValidoXML (Empresa, Ruta, Documento, Tipo, UUID, RFCEmisor, RFCReceptor, FechaTimbrado, Monto, Moneda, TipoCambio, Nombre, MonXML, Cadenaoriginal, ValidacionEstructura, CertificadoReportado, CertificadoUsado, NombreEmisor) 
                VALUES ('{empresa}', '{ruta}', '{documento}', '{tipo}', '{uuid}', '{rfc_emisor}', '{rfc_receptor}', '{fecha_timbrado}', {monto}, '{moneda}', {tipo_cambio}, '{nombre}', '{mon_xml}', '{cadenaoriginal}', '{validacion_estructura}', '{certificado_reportado}', '{certificado_usado}', '{nombre_emisor}')
            """)
            conn.commit()
            cursor.close()
            return cursor
        except pyodbc.Error as ex:
            return ex
