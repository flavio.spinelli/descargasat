import os
import zipfile
import base64
import json
import webview
import sqlite3
import datetime

from lxml import etree

from cfdiclient import Autenticacion
from cfdiclient import SolicitaDescarga
from cfdiclient import VerificaSolicitudDescarga
from cfdiclient import Fiel
from cfdiclient import DescargaMasiva

database = 'database.db'

conn = sqlite3.connect(database)
c = conn.cursor()
c.execute('''CREATE TABLE IF NOT EXISTS credentials
     (cer text, key text, password text, rfc text UNIQUE, folder text)''')
conn.close()

print('test')