import React from "react";
import {
  BrowserRouter as Router,
  Route
} from "react-router-dom";
import { AnimatedSwitch } from 'react-router-transition';
import { Fabric } from "@fluentui/react";
import Layout from "./containers/Layout/Layout";
import FacturasEmitidas from "./containers/FacturasEmitidas/FacturasEmitidas"
import FacturasRecibidas from "./containers/FacturasRecibidas/FacturasRecibidas";
import RecuperarDescargasDeCFDI from "./containers/RecuperarDescargasDeCFDI/RecuperarDescargasDeCFDI";
import Login from "./containers/Login/Login";

import '@fluentui/react/dist/css/fabric.min.css';
import './App.scss';

function Home() {
  return <h2>Descarga SAT</h2>;
}

export default function App() {
  return (
    <Fabric>
      <Router>
        <Layout>
          <AnimatedSwitch
            atEnter={{ opacity: 0 }}
            atLeave={{ opacity: 0 }}
            atActive={{ opacity: 1 }}
            className="switch-wrapper"
          >
            <Route path="/facturas-recibidas">
              <FacturasRecibidas />
            </Route>
            <Route path="/facturas-emitidas">
              <FacturasEmitidas />
            </Route>
            <Route path="/">
              <Login />
            </Route>
          </AnimatedSwitch>
        </Layout>
      </Router>
    </Fabric>
  );
}