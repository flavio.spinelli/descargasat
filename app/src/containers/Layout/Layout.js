import React, { useEffect } from 'react';
import { withRouter } from "react-router-dom";
import { Nav } from '@fluentui/react/lib/Nav';
import { IPersonaSharedProps, Persona, PersonaSize, PersonaPresence } from '@fluentui/react/lib/Persona';
import { Dialog, DialogType, DialogFooter } from '@fluentui/react/lib/Dialog';
import { PrimaryButton, DefaultButton } from '@fluentui/react/lib/Button';
import Login from '../../containers/Login/Login';
import './Layout.scss';

const navStyles = {
    root: {
      width: '100%',
      height: '100%',
      boxSizing: 'border-box',
      border: '1px solid #eee',
      overflowY: 'auto',
    },
};

const navLinkGroups = [
    {
      links: [
        {
          name: 'Facturas Emitidas',
          url: '/facturas-emitidas',
          key: 'facturas-emitidas',
          isExpanded: false
        },
        {
          name: 'Facturas Recibidas',
          url: '/facturas-recibidas',
          key: 'facturas-recibidas',
          isExpanded: false
        },
        // {
        //   name: 'Recuperar Descargas de CFDI',
        //   url: '/recuperar-descargas-de-cfdi',
        //   key: 'recuperar-descargas-de-cfdi',
        //   isExpanded: false
        // },
        {
          name: 'Salir',
          key: 'logout'
        }
      ]
    }
];

const Layout = ({ history, children, match }) => {
    const [renderDetails, updateRenderDetails] = React.useState(true);
    const [title, setTitle] = React.useState('');
    const [hideDialog, setHideDialog] = React.useState(true);

    useEffect(() => {
      let interval = setInterval(() => {
        try {
          window.pywebview.api.get_credentials()
            .then((res) => {
              try {
                setTitle(res[0][3])
              } catch {} 
              // clearInterval(interval)
            })
        } catch(err) {
          //pass
        }
      }, 1000)
    }, [])

    const onLinkClick = (e, item) => {
        e.preventDefault()
        if(item.key === 'logout') {
          setHideDialog(false)
        } else {
          history.push(item.url)
        }
    }

    const logout = () => {
      try {
        window.pywebview.api.logout()
          .then((res) => {
            setHideDialog(true)
            history.push('/')
          })
      } catch(err) {
        //pass
      }
    }

    return (
        <div className="layout">
            {
              history.location.pathname !== '/' ?
              <>
                <div className="layout-left">
                  <Persona
                    imageUrl=''
                    imageInitials={title[0]+title[1]}
                    text={title}
                    secondaryText=''
                    size={PersonaSize.size40}
                    hidePersonaDetails={!renderDetails}
                    imageAlt=""
                  />
                  <Nav
                      onLinkClick={onLinkClick}
                      selectedKey={history.location.pathname.replace('/', '')}
                      ariaLabel="Menu"
                      styles={navStyles}
                      groups={navLinkGroups}
                  />
                </div>
                <div className="layout-right">
                    {children}
                </div>
              </>
              :
              <div className="layout-full">{children}</div>
            }

          <Dialog
            hidden={hideDialog}
            onDismiss={() => setHideDialog(true)}
            dialogContentProps={{
              type: DialogType.normal,
              title: 'Salir',
              subText: '¿Quieres salir de la aplicación?',
            }}
            modalProps={{
              isBlocking: true,
              styles: { main: { maxWidth: 450 } }
            }}
          >
            <DialogFooter>
              <PrimaryButton onClick={() => logout()} text="Salir" />
              <DefaultButton onClick={() => setHideDialog(true)} text="Cancelar" />
            </DialogFooter>
          </Dialog>
        </div>
    )
}

export default withRouter(Layout);