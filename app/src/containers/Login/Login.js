import React, { useState } from 'react';
import { withRouter } from 'react-router-dom';
import { 
    TextField,
    Button,
    Stack,
    PrimaryButton
} from "@fluentui/react";
import './Login.scss';

const Login = ({history}) => {
    const [cer, setCer] = useState('')
    const [key, setKey] = useState('')
    const [password, setPassword] = useState('')
    const [rfc, setRFC] = useState('')
    const [folder, setFolder] = useState('')

    const [company, setCompany] = useState('')
    const [dbServer, setDBServer] = useState('')
    const [dbName, setDBName] = useState('')
    const [dbUser, setDBUser] = useState('')
    const [dbPassword, setDBPassword] = useState('')

    const isValid = () => {
        return (
            cer.trim() === '' ||
            key.trim() === '' ||
            password.trim() === '' ||
            rfc.trim() === '' ||
            folder.trim() === '' ||
            company.trim() === '' ||
            dbServer.trim() === '' ||
            dbName.trim() === '' ||
            dbUser.trim() === '' ||
            dbPassword.trim() === ''
        )
    }

    const getCer = () => {
        try {
            window.pywebview.api.open_file_dialog('cer')
                .then((path) => {
                    if(path) setCer(path[0])
                })
        } catch(err) {
            alert('Error')
        }
    }

    const getKey = () => {
        try {
            window.pywebview.api.open_file_dialog('key')
                .then((path) => {
                    if(path) setKey(path[0])
                })
        } catch(err) {
            alert('Error')
        }
    }

    const getFolder = () => {
        try {
            window.pywebview.api.open_folder_dialog()
                .then((path) => {
                    if(path) setFolder(path[0])
                })
        } catch(err) {
            alert('Error')
        }
    }

    const login = () => {
        window.pywebview.api.login(cer, key, password, rfc, folder, company, dbServer, dbName, dbUser, dbPassword)
            .then((res) => {
                if(res) {
                    history.push('/facturas-emitidas')
                }
            })
    }

    return (
        <div className="login">
            <div className="login-left">

            </div>
            <div className="login-right">
                <h1>Descarga SAT</h1>
                <form>
                    <h4>Datos de SAT</h4>
                    <Stack 
                        horizontal 
                        verticalAlign="end"
                        tokens={{
                            childrenGap: 15
                        }}
                    >
                        <Stack grow>
                            <TextField label="Certificado (.cer):" placeholder="Ubicación del certificado" value={cer} readOnly />
                        </Stack>
                        <Button text="Buscar" onClick={() => getCer()} />
                    </Stack>
                    <Stack 
                        horizontal 
                        verticalAlign="end"
                        tokens={{
                            childrenGap: 15
                        }}
                    >
                        <Stack grow>
                            <TextField label="Clave privada (.key):" placeholder="Ubicación de la llave privada" value={key} readOnly />
                        </Stack>
                        <Button text="Buscar" onClick={() => getKey()} />
                    </Stack>
                    <Stack grow>
                        <TextField 
                            type="password"
                            label="Contraseña de clave privada:" 
                            placeholder="Contraseña" 
                            value={password} 
                            onChange={(e) => setPassword(e.target.value)} 
                        />
                    </Stack>
                    <Stack grow>
                        <TextField 
                            label="RFC:" 
                            placeholder="RFC" 
                            value={rfc} 
                            onChange={(e) => setRFC(e.target.value)} 
                        />
                    </Stack>
                    <Stack 
                        horizontal 
                        verticalAlign="end"
                        tokens={{
                            childrenGap: 15
                        }}
                    >
                        <Stack grow>
                            <TextField label="Carpeta para guardar los XMLs" placeholder="Ubicación de la carpeta" value={folder} readOnly />
                        </Stack>
                        <Button text="Abrir" onClick={() => getFolder()} />
                    </Stack>
                    <br />
                    <hr />
                    <h4>Datos de la base de datos</h4>
                    <Stack grow>
                        <TextField 
                            label="Empresa:" 
                            placeholder="ABC" 
                            value={company} 
                            onChange={(e) => setCompany(e.target.value)} 
                        />
                    </Stack>
                    <Stack grow>
                        <TextField 
                            label="Servidor:" 
                            placeholder="IP" 
                            value={dbServer} 
                            onChange={(e) => setDBServer(e.target.value)} 
                        />
                    </Stack>
                    <Stack grow>
                        <TextField 
                            label="Nombre:" 
                            placeholder="Nombre del banco de datos" 
                            value={dbName} 
                            onChange={(e) => setDBName(e.target.value)} 
                        />
                    </Stack>
                    <Stack grow>
                        <TextField 
                            label="Usuario:" 
                            placeholder="Nombre de usuario" 
                            value={dbUser} 
                            onChange={(e) => setDBUser(e.target.value)} 
                        />
                    </Stack>
                    <Stack grow>
                        <TextField 
                            type="password"
                            label="Contraseña:" 
                            placeholder="Contraseña" 
                            value={dbPassword} 
                            onChange={(e) => setDBPassword(e.target.value)} 
                        />
                    </Stack>
                    <br />
                    <Stack>
                        <PrimaryButton text="Entrar" disabled={isValid()} onClick={() => login()} />
                    </Stack>
                </form>
            </div>
        </div>
    )
}

export default withRouter(Login);