import React from 'react';
import { 
    Text,
    Pivot,
    PivotItem,
    Stack,
    ComboBox,
    PrimaryButton,
    ProgressIndicator
} from "@fluentui/react";
import { TextField } from '@fluentui/react/lib/TextField';
import { FontSizes, FontWeights } from '@fluentui/theme';
import InputMask from 'react-input-mask';
import Datetime from 'react-datetime';
import "react-datetime/css/react-datetime.css";
import './FacturasEmitidas.scss';

let interval

const FacturasEmitidas = () => {
    const [percentComplete, setPercentComplete] = React.useState(0);
    const [progressStatus, setProgressStatus] = React.useState('');
    const [fechaInicial, setFechaInicial] = React.useState('');
    const [fechaFinal, setFechaFinal] = React.useState('');
    const [rfcReceptor, setRFCReceptor] = React.useState('');
    const [estadoDelComprobante, setEstadoDelComprobante] = React.useState('');
    const [tipoDeComprobante, setTipoDeComprobante] = React.useState('');
    const [showProgress, setShowProgress] = React.useState(false);

    const setProgress = (value, increment, maxValue) => {
        interval = setInterval(() => {
            value += increment
            if(value >= maxValue) clearInterval(interval)
            setPercentComplete(value)
        }, 100)
    }

    const getStatus = (status) => {
        switch(status) {
            case '1':
                return 'Aceptada';
            case '2':
                return 'En proceso';
            case '3':
                return 'Terminada';
            case '4':
                return 'Error';
            case '5':
                return 'Rechazada';
            case '6':
                return 'Vencida';
            default:
                return '';
        }
    }

    const solicitaDescarga = () => {
        setShowProgress(true)
        setProgressStatus('Solicitando descarga')

        window.pywebview.api.solicita_descarga('emitidas', fechaInicial, fechaFinal, rfcReceptor)
            .then((res) => {
                let id_solicitud = res['id_solicitud']
                let status = res['cod_estatus']
                if(status === '5002') {
                    setShowProgress(false)
                    alert('SAT: '+ res['mensaje'])
                    return false;
                }
                setProgressStatus('Verificando solicitud de descarga')

                let first = true
                let wait = setInterval(() => {
                    window.pywebview.api.verifica_solicitud_de_descarga(id_solicitud)
                        .then((res) => {
                            // setProgressStatus(`Solicitud de descarga ${getStatus(res.estado_solicitud)}`)
                            
                            if(parseInt(res.estado_solicitud) <= 2) {
                                setProgressStatus('Aguardando respuesta de SAT')
                            }
                            else if(parseInt(res.estado_solicitud) >= 4) {
                                setShowProgress(false)
                                alert('SAT:'+ res.mensaje)
                                return false;
                            } else {
                                if(first) {
                                    first = false
                                }
                                
                                clearInterval(wait)
    
                                let paquetes = res.paquetes;
                                // baixar
                                setProgressStatus(`Descargando paquetes`)
                                paquetes.forEach((paquete, key) => {

                                    let downloads = []
                        
                                    window.pywebview.api.descarga(paquete)
                                        .then((res) => {
                                            if(res) {
                                                setProgressStatus(`Descarga lista del paquete ${paquete}`)
                                                downloads.push(res)
                                                
                                                if(key+1 === paquetes.length) {
                                                    setProgressStatus(`Extrayendo archivos de los paquetes`)

                                                    downloads.forEach((download, key2) => {
                                                
                                                        window.pywebview.api.unzip_file(download.file_name, download.dir_name)
                                                            .then((res2) => {
                                                                if(res2 !== true) {
                                                                    setShowProgress(false)
                                                                    alert('Error: '+ res2)
                                                                    return false;
                                                                }

                                                                setProgressStatus(`Archivos extraídos del paquete ${download.file_name.replace('.zip', '')}`)
    
                                                                if(key2+1 === downloads.length) {
                                                                    setProgressStatus(`Leyendo los archivos y insertando en la base de datos`)

                                                                    downloads.forEach((download, key3) => {
                                                                        window.pywebview.api.read_and_insert(download.dir_name +'/'+ download.file_name.replace('.zip', ''))
                                                                            .then((res3) => {
                                                                                setProgressStatus(`Archivos procesados ​​desde el paquete ${download.file_name.replace('.zip', '')}`)

                                                                                if(key3+1 === downloads.length)  {
                                                                                    setProgressStatus('Descarga completada con éxito.')
                                                                                    setShowProgress(false)
                                                                                    // setFechaInicial('')
                                                                                    // setFechaFinal('')
                                                                                    // setRFCReceptor('')
                                                                                    // setEstadoDelComprobante('')
                                                                                    // setTipoDeComprobante('')
                                                                                    // setShowProgress('')

                                                                                    alert('Descarga completada con éxito.')
                                                                                    return true;
                                                                                }
                                                                            })
                                                                    })
                                                                }
                                                            })
                                                            .catch((err) => {
                                                                setShowProgress(false)
                                                                alert('Unzip error')
                                                                return false;
                                                            }) 
                                                    })         
                                                }
                                            }
                                        })
                                        .catch((err) => {
                                            setShowProgress(false)
                                            alert('Error: '+ JSON.stringify(err))
                                            return false;
                                        })
                                })

                            }
                        })
                        .catch((err) => {
                            setShowProgress(false)
                            alert('Error: '+ JSON.stringify(err))
                            return false;
                        })
                }, 3000)
            })
            .catch((err) => {
                setShowProgress(false)
                alert('Error: '+ JSON.stringify(err))
                return false;
            })
    }

    return (
        <div className="facturas-emitidas">
            <Text style={{ fontSize: FontSizes.size32, fontWeight: FontWeights.regular }}>Facturas Emitidas</Text>

            <Pivot aria-label="Basic Pivot Example">
                {/* <PivotItem headerText="Folio Fiscal">
                    <br />
                    <InputMask mask="99999999-9999-9999-9999-999999999999">
                        {(inputProps) =>  <TextField {...inputProps} label="Folio Fiscal *" />}
                    </InputMask>

                    <br />
                    <PrimaryButton text="Buscar CFDI" disabled={percentComplete > 0 && percentComplete < 1} onClick={() => solicitaDescarga()} />
                </PivotItem> */}
                <PivotItem headerText="Fecha de Emisíon" headerText="">

                    <Stack 
                        horizontal
                        tokens={{
                            childrenGap: 30
                        }}
                    >
                        <Stack grow>
                            <label for="fecha-inicial" id="fecha-inicial-label" className="ms-Label root-128">Fecha Inicial</label>
                            <div className="ms-TextField-fieldGroup fieldGroup-117">
                                <Datetime id="fecha-inicial" className="ms-TextField-field field-118 fieldGroup-118" value={fechaInicial} dateFormat="YYYY-MM-DD HH:mm:ss" onChange={(e) => setFechaInicial(e.format('YYYY-MM-DD HH:mm:ss'))} /> {/* 2021-01-28 00:00:00 */}
                            </div>
                        </Stack>
                        <Stack grow>
                            <label for="fecha-final" id="fecha-final-label" className="ms-Label root-128">Fecha Final</label>
                            <div className="ms-TextField-fieldGroup fieldGroup-117">
                                <Datetime id="fecha-final" className="ms-TextField-field field-118 fieldGroup-118" value={fechaFinal} dateFormat="YYYY-MM-DD HH:mm:ss" onChange={(e) => setFechaFinal(e.format('YYYY-MM-DD HH:mm:ss'))} />
                            </div>
                        </Stack>
                    </Stack>
                    <Stack 
                        horizontal
                        tokens={{
                            childrenGap: 30
                        }}
                    >
                        <Stack grow>
                            <TextField label="RFC Receptor" value={rfcReceptor} onChange={(e) => setRFCReceptor(e.target.value)} />
                        </Stack>
                        <Stack grow>
                            <ComboBox 
                                label="Estado del Comprobante" 
                                placeholder="Seleccione un valor..."
                                value={estadoDelComprobante}
                                onChange={(e, selectedOption) => setEstadoDelComprobante(selectedOption)}
                                options={[
                                    { key: '0', text: 'Cancelado' },
                                    { key: '1', text: 'Vigente' }
                                ]}
                            />
                        </Stack>
                    </Stack>
                    <Stack 
                        horizontal
                        tokens={{
                            childrenGap: 30
                        }}
                    >
                        <Stack grow>
                            <ComboBox 
                                label="Tipo de Comprobante (Complemento)" 
                                placeholder="Seleccione un valor..."
                                value={tipoDeComprobante}
                                onChange={(e, selectedOption) => setTipoDeComprobante(selectedOption)}
                                options={[
                                    { key: '8', text: 'Estándar' },
                                    { key: '4294967296', text: 'Acreditamiento de IEPS' },
                                    { key: '8388608', text: 'Aerolíneas' },
                                    { key: '70368744177664', text: 'Carta Porte' },
                                    { key: '1073741824', text: 'Certificado de Destrucción' },
                                    { key: '17179869184', text: 'Comercio Exterior' },
                                    { key: '274877906944', text: 'Comercio Exterior 1.1' },
                                    { key: '4', text: 'Compra Venta de Divisas' },
                                    { key: '16777216', text: 'Consumo de Combustibles' },
                                    { key: '8796093022208', text: 'Consumo De Combustibles 1.1' },
                                    { key: '64', text: 'Donatarias' },
                                    { key: '256', text: 'Estado De Cuenta Bancario' },
                                    { key: '4398046511104', text: 'Estado de Cuenta Combustibles 1.2' },
                                    { key: '8589934592', text: 'Estado de cuenta de combustibles de monederos electrónicos.' },
                                    { key: '17592186044416', text: 'Gastos de Hidrocarburos' },
                                    { key: '68719476736', text: 'INE 1.1' },
                                    { key: '35184372088832', text: 'Ingresos de Hidrocarburos' },
                                    { key: '1024', text: 'Instituciones Educativas Privadas (Pago de colegiatura)' },
                                    { key: '4096', text: 'Leyendas Fiscales' },
                                    { key: '524288', text: 'Mis Cuentas' },
                                    { key: '67108864', text: 'Notarios Públicos' },
                                    { key: '536870912', text: 'Obras de artes y antigüedades' },
                                    { key: '2048', text: 'Otros Derechos e Impuestos' },
                                    { key: '4194304', text: 'Pago en Especie' },
                                    { key: '8192', text: 'Persona Física Integrante de Coordinado' },
                                    { key: '549755813888', text: 'Recepción de Pagos' },
                                    { key: '128', text: 'Recibo de donativo' },
                                    { key: '1048576', text: 'Recibo de Pago de Salarios' },
                                    { key: '137438953472', text: 'Recibo de Pago de Salarios 1.2' },
                                    { key: '32', text: 'Sector de Ventas al Detalle (Detallista)' },
                                    { key: '268435456', text: 'Servicios de construcción' },
                                    { key: '16384', text: 'SPEI de Tercero a Tercero' },
                                    { key: '2147483648', text: 'Sustitución y renovación vehicular' },
                                    { key: '32768', text: 'Terceros' },
                                    { key: '65536', text: 'Terceros' },
                                    { key: '2199023255552', text: 'Timbre Fiscal Digital' },
                                    { key: '16', text: 'Turista o Pasajero Extranjero' },
                                    { key: '33554432', text: 'Vales de Despensa' },
                                    { key: '134217728', text: 'Vehículo Usado' },
                                    { key: '2097152', text: 'Venta de Vehiculos' }
                                ]}
                            />
                            <small>(Criterio de búsqueda aplicable a CFDI emitidas a partir del 01/01/2014)</small>
                        </Stack>
                        <Stack grow>&nbsp;</Stack>
                    </Stack>

                    <br />
                    <PrimaryButton text="Buscar CFDI" disabled={(fechaInicial === '' || fechaFinal === '')} onClick={() => solicitaDescarga()} />
                </PivotItem>
            </Pivot>

            <br />
            <br />
            {showProgress ? 
            <ProgressIndicator label="Proceso de descarga" description={progressStatus}  />
            : null}

        </div>
    )
}

export default FacturasEmitidas;