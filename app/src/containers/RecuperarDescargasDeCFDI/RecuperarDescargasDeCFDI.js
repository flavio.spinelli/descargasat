import React from 'react';
import { Text } from "@fluentui/react";
import { FontSizes, FontWeights } from '@fluentui/theme';

const RecuperarDescargasDeCFDI = () => {
    return (
        <div>
            <Text style={{ fontSize: FontSizes.size32, fontWeight: FontWeights.regular }}>Recuperar Descargas de CFDI</Text>
        </div>
    )
}

export default RecuperarDescargasDeCFDI;