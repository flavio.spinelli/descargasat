# -*- coding: utf-8 -*-
from cfdiclient import VerificaSolicitudDescarga
from cfdiclient import Fiel
import os

##
## Constantes de Loggin
##
RFC = 'GACJ8504306G7'
FIEL_CER = '00001000000506197164.cer'
FIEL_KEY = 'Claveprivada_FIEL_GACJ8504306G7_20210117_114311.key'
FIEL_PAS = '8nalgwpJ$'
PATH = 'certificados/'

cer_der = open(os.path.join(PATH, FIEL_CER), 'rb').read()
key_der = open(os.path.join(PATH, FIEL_KEY), 'rb').read()

fiel = Fiel(cer_der, key_der, FIEL_PAS)

v_descarga = VerificaSolicitudDescarga(fiel)

token = 'eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOjE2MjU4NDU5MjIsImV4cCI6MTYyNTg0NjUyMiwiaWF0IjoxNjI1ODQ1OTIyLCJpc3MiOiJMb2FkU29saWNpdHVkRGVjYXJnYU1hc2l2YVRlcmNlcm9zIiwiYWN0b3J0IjoiMzAzMDMwMzAzMTMwMzAzMDMwMzAzMDM1MzAzNjMxMzkzNzMxMzYzNCJ9.ChaNePbrp0UWdk_-fzV9tX85rk5Ed6QGNeXGpvpDb8Q%26wrap_subject%3d3030303031303030303030353036313937313634'
rfc_solicitante = 'GACJ8504306G7'
id_solicitud = '10b82f09-811c-4c31-b096-532610440679'
result = v_descarga.verificar_descarga(token, rfc_solicitante, id_solicitud)
print(result)
