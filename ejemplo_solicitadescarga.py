# -*- coding: utf-8 -*-
import datetime
from cfdiclient import SolicitaDescarga
from cfdiclient import Fiel
import os

##
## Constantes de Loggin
##
RFC = 'GACJ8504306G7'
FIEL_CER = '00001000000506197164.cer'
FIEL_KEY = 'Claveprivada_FIEL_GACJ8504306G7_20210117_114311.key'
FIEL_PAS = '8nalgwpJ$'
PATH = 'certificados/'

cer_der = open(os.path.join(PATH, FIEL_CER), 'rb').read()
key_der = open(os.path.join(PATH, FIEL_KEY), 'rb').read()

fiel = Fiel(cer_der, key_der, FIEL_PAS)

descarga = SolicitaDescarga(fiel)

token = 'eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTI1NiIsInR5cCI6IkpXVCJ9.eyJuYmYiOjE2MjU4NDU5MjIsImV4cCI6MTYyNTg0NjUyMiwiaWF0IjoxNjI1ODQ1OTIyLCJpc3MiOiJMb2FkU29saWNpdHVkRGVjYXJnYU1hc2l2YVRlcmNlcm9zIiwiYWN0b3J0IjoiMzAzMDMwMzAzMTMwMzAzMDMwMzAzMDM1MzAzNjMxMzkzNzMxMzYzNCJ9.ChaNePbrp0UWdk_-fzV9tX85rk5Ed6QGNeXGpvpDb8Q%26wrap_subject%3d3030303031303030303030353036313937313634'
rfc_solicitante = 'GACJ8504306G7'
fecha_inicial = datetime.datetime(2021, 1, 1)
fecha_final = datetime.datetime(2021, 1, 30)
rfc_emisor = 'GACJ8504306G7'
rfc_receptor = 'GACJ8504306G7'
# Emitidos
result = descarga.solicitar_descarga(token, rfc_solicitante, fecha_inicial, fecha_final, rfc_emisor=rfc_emisor)
print(result)
# Recibidos
result = descarga.solicitar_descarga(token, rfc_solicitante, fecha_inicial, fecha_final, rfc_receptor=rfc_receptor)
print(result)
